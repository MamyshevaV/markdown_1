def itallic (text: str):
"""
    Функция делает курсивный текст
"""
    if text.startswith('*') and text.endswith('*'):
        res = '<em>' + text[1:-1] + '</em>'
        return res


def bold (text: str):
"""
    Функция делает полужирный текст
"""
    if text.startswith('**') and text.endswith('**') and (text.count('*') != 2):
        res = '<strong>' + text[2:-2] + '</strong>'
        return res


print('Программа, преобразующая строку в полужирный или курсивный текст.')
while True:
    start = input('''Введите help, если требуется пояснения,
введите continue, чтобы начать ввод текста,
введите finish, чтобы закончить:\n''')

    if start == 'help':
        help = {
            'Строка обрамлена двумя звездочками': ('текст будет выделен полужирным начертанием,'),
            'Строка обрамлена одной звездочкой': ('текст будет выделен курсивным наклоном,'),
            'Строка без звездочек': ('текст будет неизменным.\n'),
         }
        for k, v in help.items():
            print(f'{k} - {v}')

    if start == 'continue':
        while True:
            S = input('Введите строку, которую хотите преобразовать: ')
            if (S.count('*')  != 3):
                if bold(S) != None:
                    result = bold(S)
                    print('\nРезультат работы программы: ', result, '\n')
                elif itallic(S) != None:
                    result = itallic(S)
                    print('\nРезультат работы программы: ', result, '\n')
                else: 
                    print('\nРезультат работы программы: ', S, '\n')
            else:
                print('Неправильное оформление текста, попробуйте еще раз.')
            if S == 'finish':
                break
    if start == 'finish':
        break
